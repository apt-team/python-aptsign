#!/usr/bin/python3
#
# Copyright (C) 2021 Julian Andres Klode <jak@debian.org>
#
# SPDX-License-Identifier: MIT

import argparse
import base64
import os
import pathlib
import struct
import sys
import typing

import cryptography.hazmat.primitives.asymmetric.ed25519
import cryptography.hazmat.primitives.serialization

from . import verify


class PrivateKey:
    def __init__(self, private_key: bytes, subkey: typing.Optional[bytes]):
        self.private_key = private_key
        self.subkey = subkey
        self._object = cryptography.hazmat.primitives.asymmetric.ed25519.Ed25519PrivateKey.from_private_bytes(
            private_key
        )

    @staticmethod
    def from_file(path: os.PathLike[str]) -> "PrivateKey":
        private_key = None
        subkey = None
        with open(path) as fobj:
            for line in fobj:
                if not line.strip():
                    continue
                key, value = line.split(": ")
                if key == "private-key":
                    private_key = base64.b64decode(value)
                elif key == "subkey":
                    subkey = base64.b64decode(value)
        if private_key is None:
            raise ValueError("No private key in the file")
        return PrivateKey(private_key, subkey)

    def to_file(self, path: os.PathLike[str]) -> None:
        with open(path, "w") as fobj:
            print(
                "private-key:",
                base64.b64encode(self.private_key).decode("utf-8"),
                file=fobj,
            )
            if self.subkey is not None:
                print(
                    "subkey: ", base64.b64encode(self.subkey).decode("utf-8"), file=fobj
                )
            print(file=fobj)

    @staticmethod
    def new() -> "PrivateKey":
        with open("/dev/random", "rb") as random:
            return PrivateKey(random.read(32), subkey=None)

    def public_key(self) -> bytes:
        return self._object.public_key().public_bytes(
            encoding=cryptography.hazmat.primitives.serialization.Encoding.Raw,
            format=cryptography.hazmat.primitives.serialization.PublicFormat.Raw,
        )

    def generate_subkey(self, expiry: int, serial: int) -> "PrivateKey":
        subkey = PrivateKey.new()
        subkey.subkey = self.sign_subkey(
            subkey.public_key(), expiry=expiry, serial=serial
        )
        return subkey

    def sign_subkey(self, public_key: bytes, expiry: int, serial: int) -> bytes:
        if self.subkey is not None:
            raise ValueError("Subkeys cannot have subkeys")

        payload = public_key + struct.pack("<QL", expiry, serial)
        assert len(public_key) == 32
        assert len(payload) == 32 + 8 + 4
        result = self.public_key() + payload + self._object.sign(payload)
        assert len(result) == 32 + (32 + 8 + 4) + 64

        assert len(result) == verify.Subkey.LENGTH
        return result

    def sign(self, message: bytes) -> verify.Signature:
        signature = self._object.sign(message)
        assert len(signature) == 64
        if self.subkey is not None:
            assert len(self.subkey) == verify.Subkey.LENGTH
            return verify.Signature(self.subkey + signature, key_format=verify.Subkey)
        return verify.Signature(
            self.public_key() + signature, key_format=verify.PublicKey
        )


def _register_subcommands(subparsers: argparse._SubParsersAction) -> None:
    def generate_primary(args: argparse.Namespace) -> None:
        priv = PrivateKey.new()
        priv.to_file(args.primary_key)

    def generate_subkey(args: argparse.Namespace) -> None:
        primary = PrivateKey.from_file(args.primary_key)
        subkey = primary.generate_subkey(
            expiry=args.expires,
            serial=args.serial,
        )
        subkey.to_file(args.subkey)

    def sign_file(args: argparse.Namespace) -> None:
        private_key = PrivateKey.from_file(args.private_key)
        file_to_sign = verify.SignedFile()
        file_to_sign.read_file(sys.stdin.buffer)
        file_to_sign.signatures.append(private_key.sign(file_to_sign.buffer))

        sys.stdout.buffer.write(file_to_sign.buffer)
        sys.stdout.buffer.write(b"Signatures:\n")
        for signature in file_to_sign.signatures:
            sys.stdout.buffer.write(b" ")
            sys.stdout.buffer.write(bytes(signature))
            sys.stdout.buffer.write(b"\n")
        sys.stdout.buffer.write(b"\n")

    def public_key(args: argparse.Namespace) -> None:
        private_key = PrivateKey.from_file(args.private_key)
        print(base64.b64encode(private_key.public_key()).decode("utf-8"))

    subparser = subparsers.add_parser(
        "generate-primary", help="Generate a new primary key"
    )
    subparser.add_argument("primary_key", type=pathlib.Path)
    subparser.set_defaults(func=generate_primary)

    subparser = subparsers.add_parser("generate-subkey", help="Generate a new subkey")
    subparser.add_argument("primary_key", type=pathlib.Path)
    subparser.add_argument("subkey", type=pathlib.Path)
    subparser.add_argument(
        "--expires", type=int, default=2 ** 64 - 1, help="Secons since epoch"
    )
    subparser.add_argument("--serial", type=int, default=0, help="Serial number")
    subparser.set_defaults(func=generate_subkey)

    subparser = subparsers.add_parser(
        "sign-file",
        help="Sign a release file on stdin with the given primary key or subkey, result on stdout",
    )
    subparser.add_argument("private_key", type=pathlib.Path)
    subparser.set_defaults(func=sign_file)

    subparser = subparsers.add_parser(
        "public-key",
        help="Return public key of private key on stdout",
    )
    subparser.add_argument("private_key", type=pathlib.Path)
    subparser.set_defaults(func=public_key)


def main() -> None:
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(
        title="subcommands", required=True, dest="command"
    )

    _register_subcommands(subparsers)
    args = parser.parse_args()
    args.func(args)


if __name__ == "__main__":
    main()
