#!/usr/bin/python3
#  apt-sign reference implementation
#
# Copyright (C) 2021 Julian Andres Klode <jak@debian.org>
#
# SPDX-License-Identifier: MIT
"""Entry point for the program."""

import argparse


def main() -> None:
    from . import sign, verify

    parser = argparse.ArgumentParser("aptsign", description="Ed25519 signature tool")
    subparsers = parser.add_subparsers(
        title="subcommands", required=True, dest="command"
    )

    subparser = subparsers.add_parser("verify-file", help=verify.main.__doc__)
    subparser.set_defaults(func=verify.main)

    sign._register_subcommands(subparsers)
    args = parser.parse_args()
    args.func(args)


if __name__ == "__main__":
    main()
