#!/usr/bin/python3
#  apt-sign reference implementation
#
# Copyright (C) 2021 Julian Andres Klode <jak@debian.org>
#
# SPDX-License-Identifier: MIT

"""
Reference implementation of the apt-sign ed25519 signature verification

This supports signature verification on Python 3 using python-cryptography. It supports
signatures from subkeys as well as signatures from raw Ed25519 keys.
"""

import argparse
import base64
import struct
import sys
import time
import typing
from abc import ABCMeta, abstractmethod

import cryptography.hazmat.primitives.asymmetric.ed25519
import cryptography.hazmat.primitives.serialization
from cryptography.exceptions import InvalidSignature


class SigningKey(metaclass=ABCMeta):
    """Basic interface for a signing key."""

    LENGTH: typing.ClassVar[int]
    RAWSIG_LENGTH: typing.ClassVar[int]

    @abstractmethod
    def __init__(self, blob: bytes):
        pass

    @abstractmethod
    def verify(self, message: bytes, signature: bytes, minimum_serial: int = 0) -> None:
        """Verify the given message using the given signature."""
        raise NotImplementedError("Subclass must implement verify")


class PublicKey(SigningKey):
    """Simple 32-byte public Ed25519 key."""

    LENGTH = 32
    RAWSIG_LENGTH = 64

    def __init__(self, blob: bytes):
        super().__init__(blob)
        if len(blob) != 32:
            raise ValueError("A public key must be 32 bytes long")
        self._blob = blob
        self._object = cryptography.hazmat.primitives.asymmetric.ed25519.Ed25519PublicKey.from_public_bytes(
            blob
        )

    def verify(self, message: bytes, signature: bytes, minimum_serial: int = 0) -> None:
        self._object.verify(signature, message)

    def __str__(self) -> str:
        return base64.b64encode(self._blob).decode("utf-8")


class Subkey(SigningKey):
    """
    Signed subkey consisting of:

    - The public key of the primary key
    - Signed subkey payload: The subkey's public key, expiry time, and serial number
    - The signature of the signed subkey payload
    """

    _PRIMARY = slice(0, PublicKey.LENGTH)
    _PAYLOAD = slice(_PRIMARY.stop, _PRIMARY.stop + PublicKey.LENGTH + 8 + 4)
    _SIGNATURE = slice(_PAYLOAD.stop, _PAYLOAD.stop + PublicKey.RAWSIG_LENGTH)

    LENGTH = _SIGNATURE.stop
    RAWSIG_LENGTH = PublicKey.RAWSIG_LENGTH

    public_key: PublicKey
    expiry: int
    serial: int

    def __init__(self, blob: bytes):
        super().__init__(blob)
        if len(blob) != self.LENGTH:
            raise ValueError(f"A subkey must be {self.LENGTH} bytes long")

        self._signature = Signature(
            blob[self._PRIMARY] + blob[self._SIGNATURE], key_format=PublicKey
        )
        self._payload = blob[self._PAYLOAD]
        self.public_key = PublicKey(self._payload[: PublicKey.LENGTH])
        (self.expiry, self.serial) = struct.unpack(
            "<QL", self._payload[PublicKey.LENGTH :]
        )

    def verify(self, message: bytes, signature: bytes, minimum_serial: int = 0) -> None:
        try:
            self._signature.verify(self._payload)
        except InvalidSignature as err:
            raise InvalidSignature("Could not verify subkey") from err

        if self.serial < minimum_serial:
            raise InvalidSignature("Serial is too small")
        if self.expiry < time.time():
            raise InvalidSignature("Key is expired")

        self.public_key.verify(message, signature)

    def __str__(self) -> str:
        return f"{self._signature.signing_key}:{self.public_key}"


class Signature:
    """Signature is a subkey and a standard 64-byte raw Ed25519 signature"""

    signing_key: SigningKey
    signature: bytes

    def __init__(self, blob: bytes, key_format: typing.Type[SigningKey] = Subkey):
        self._blob = blob
        length = key_format.LENGTH + key_format.RAWSIG_LENGTH
        if len(blob) != length:
            raise ValueError(
                f"A signature must be {length} bytes long, is {len(blob)} bytes long"
            )

        self.signing_key = key_format(blob[: key_format.LENGTH])
        self.signature = blob[key_format.LENGTH :]

    def verify(self, message: bytes, minimum_serial: int = 0) -> None:
        return self.signing_key.verify(
            message, self.signature, minimum_serial=minimum_serial
        )

    def __bytes__(self) -> bytes:
        return base64.b64encode(self._blob)


class SignedFile:
    def __init__(self) -> None:
        self.buffer = b""
        self.signatures: typing.List[Signature] = []

    def read_file(self, lines: typing.Iterable[bytes]) -> None:
        """Read the given file"""
        in_signatures = False

        for line in lines:
            # Lines consisting of just whitespace end a section, we only
            # allow one section.
            if not line.strip():
                break

            in_signatures = in_signatures and line.startswith(b" ")

            if line == b"Signatures:\n":
                in_signatures = True
            elif in_signatures:
                try:
                    self.signatures.append(Signature(base64.b64decode(line.strip())))
                except ValueError:
                    self.signatures.append(
                        Signature(base64.b64decode(line.strip()), key_format=PublicKey)
                    )
            else:
                self.buffer += line

        for line in lines:
            print("E: Unexpected second section", file=sys.stderr)
            sys.exit(1)


def main(args: typing.Optional[argparse.Namespace] = None) -> None:
    """Verify file on stdin, report result on stderr, output signed content on stdout"""
    signedfile = SignedFile()
    signedfile.read_file(sys.stdin.buffer)

    error = not bool(signedfile.signatures)
    for signature in signedfile.signatures:
        try:
            signature.verify(signedfile.buffer)
        except InvalidSignature as err:
            print(
                f"E: Failed signature from {signature.signing_key}",
                "-",
                err,
                file=sys.stderr,
            )
            error = True
        else:
            print(
                f"I: Verified signature from {signature.signing_key}",
                file=sys.stderr,
            )

    if error:
        sys.exit(1)

    sys.stdout.buffer.write(signedfile.buffer)
    sys.stdout.buffer.write(b"\n")


if __name__ == "__main__":
    main()
